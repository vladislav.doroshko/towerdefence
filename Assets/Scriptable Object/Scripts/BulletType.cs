using UnityEngine;

namespace Stylad
{
    [CreateAssetMenu(fileName = "Default Bullet", menuName = "Game Types/Bullet")]
    public class BulletType : ScriptableObject
    {
        #region Private Fields

        [SerializeField] private float bulletSpeed;
        [SerializeField] private float bulletDamage;
        [SerializeField] private float bulletMinusArmor;

        #endregion

        #region Public Methods

        public float BulletSpeed => bulletSpeed;
        public float BulletDamage => bulletDamage;
        public float BulletMinusArmor => bulletMinusArmor;

        #endregion
    }
}

