using UnityEngine;

namespace Stylad
{
    [CreateAssetMenu(fileName = "Default Enemy", menuName = "Game Types/Enemy")]
    public class EnemyType : ScriptableObject
    {
        #region Private Fields

        [SerializeField] private float healthPoints;
        [SerializeField] private float armorPoints;
        [SerializeField] private float moveSpeed;

        #endregion

        #region Public Fields

        public float HealthPoints => healthPoints;
        public float ArmorPoints => armorPoints;
        public float MoveSpeed => moveSpeed;

        #endregion
    }
}

