using UnityEngine;

namespace Stylad
{
    [CreateAssetMenu(fileName = "Default Tower", menuName = "Game Types/Tower")]
    public class TowerType : ScriptableObject
    {
        #region Private Fields

        [SerializeField] private Bullet bulletPrefab;
        [SerializeField] private float shootDelay;

        #endregion

        #region Public Fields

        public Bullet BulletPrefab => bulletPrefab;
        public float ShootDelay => shootDelay;

        #endregion
    }
}

