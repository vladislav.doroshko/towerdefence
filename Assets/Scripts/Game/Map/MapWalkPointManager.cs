using System;
using System.Collections.Generic;
using UnityEngine;

namespace Stylad
{
    public class MapWalkPointManager : MonoBehaviour
    {
        #region Private Fields

        private List<CircleCollider2D> _points = new List<CircleCollider2D>();

        #endregion

        #region Events

        public Func<List<CircleCollider2D>> WalkablePoints;

        #endregion

        #region MonoBehaviour Callbacks

        private void Awake()
        {
            FindWalkablePoints();
            WalkablePoints += () => _points;
        }
        
        #endregion

        #region Private Methods

        private void FindWalkablePoints()
        {
            var tempPoints = GetComponentsInChildren<CircleCollider2D>();
            foreach (var point in tempPoints)
            {
                _points.Add(point);
            }
        }

        #endregion
    }
}

