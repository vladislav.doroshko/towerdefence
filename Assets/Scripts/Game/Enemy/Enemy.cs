using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Stylad
{
    [RequireComponent(typeof(Rigidbody2D), typeof(CircleCollider2D), typeof(SpriteRenderer))]
    public class Enemy : MonoBehaviour
    {
        #region Private Fields
        
        private float _currentHealth;
        private int _walkPointIndex;

        private List<CircleCollider2D> _walkPoints = new List<CircleCollider2D>();

        #endregion

        #region Private Components

        [SerializeField] private EnemyType enemyType;
        
        private MapWalkPointManager _mapManager;
        private Rigidbody2D _enemyRb;

        #endregion

        #region Events

        public Func<float> EnterBulletInfo;
        public Func<bool> BulletDamageTaken;

        #endregion

        #region MonoBehaviour Callbacks

        private void Awake()
        {
            _enemyRb = GetComponent<Rigidbody2D>();
            _mapManager = FindObjectOfType<MapWalkPointManager>();
            _currentHealth = enemyType.HealthPoints;
        }

        private void Start()
        {
            _walkPoints = _mapManager.WalkablePoints.Invoke();
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other != null)
            {
                if (other.TryGetComponent(out Bullet bullet))
                {
                    StartCoroutine(TakeDamageRoutine());
                }

                if (other.gameObject.CompareTag("WalkPoint"))
                {
                    _walkPointIndex++;
                }
            }
        }

        private void FixedUpdate()
        {
            Move();
        }

        #endregion

        #region Private Methods

        private void TakeDamageFromBullet(Func<float> bulletDamage)
        {
            _currentHealth -= bulletDamage.Invoke();
        }

        private void Move()
        {
            Vector2 moveDirection = (_walkPoints[_walkPointIndex].transform.position - transform.position).normalized;
            _enemyRb.velocity = moveDirection * enemyType.MoveSpeed;
        }

        #endregion

        #region Coroutines

        private IEnumerator TakeDamageRoutine()
        {
            yield return new WaitUntil(BulletDamageTaken);
            TakeDamageFromBullet(EnterBulletInfo);
        }

        #endregion
    }
}