using UnityEngine;

namespace Stylad
{
    [RequireComponent(typeof(Rigidbody2D), typeof(CircleCollider2D), typeof(SpriteRenderer))]
    public class Bullet : MonoBehaviour
    {
        #region Private Fields

        [SerializeField] private BulletType bulletType;

        #endregion

        #region Private Components

        private Rigidbody2D _bulletRb;

        #endregion

        #region Events
        

        #endregion

        #region MonoBehaviour Callbacks

        private void Start()
        {
            _bulletRb = GetComponent<Rigidbody2D>();
            _bulletRb.velocity = Vector2.up;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other != null)
            {
                if (other.TryGetComponent(out Enemy enemy))
                {
                    SubscribeToEvents(enemy);
                    Destroy(gameObject);
                }
            }
        }

        #endregion

        #region Private Methods

        private void SubscribeToEvents(Enemy enemy)
        {
            enemy.EnterBulletInfo += () => bulletType.BulletDamage;
            enemy.BulletDamageTaken = () => true;
        }

        #endregion
    }
}

